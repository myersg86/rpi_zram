### Enabling ZRAM for swap

Using ZRAM as swap is another option to increase the performance of your Raspberry Pi device **without** requiring an proper harddrive.

ZRAM uses compression algorithms and creates a block device which can be used for swap or as a general-purpose RAM disk. Using ZRAM for swap results in a noticeable improvement in the performance of GitLab on Raspberry Pi 2/3.

To optimize the RaspberryPi for ZRAM and automatically enable it on reboot, we can use a shell script.

```sh
sudo nano /usr/bin/zram.sh
```

```bash
#!/bin/bash
cores=$(nproc --all)
modprobe zram num_devices=$cores

swapoff -a

totalmem=`free | grep -e "^Mem:" | awk '{print $2}'`
mem=$(( ($totalmem / $cores)* 1024 ))

core=0
while [ $core -lt $cores ]; do
  echo $mem > /sys/block/zram$core/disksize
  mkswap /dev/zram$core
  swapon -p 5 /dev/zram$core
  let core=core+1
done
```

This script creates an additional compressed RAM swap disk for each CPU core and activates it. No further configuration action is necessary.

You can also download this script with the following command:

```
sudo wget -O /usr/bin/zram.sh https://gitlab.com/myersg86/rpi_zram/raw/master/zram.sh
```

Make the script executable:

```sh
sudo chmod +x /usr/bin/zram.sh
```

Run `zram.sh` with the command:

```
sudo zram.sh
```

By running *swap -s* we can see that we have four additional swap space partitions created and running: `/dev/zram0`, `/dev/zram1/`, `/dev/zram2/`, and `/dev/zram3/` 

To automatically enable ZRAM at boot time, edit the `/etc/rc.local` file 

```sh
sudo nano /etc/rc.local
```

and insert the line

`/usr/bin/zram.sh & `

at the end of the file, but before the final

 `exit 0` 

line of the file.

The final two lines should look like:

```sh
/usr/bin/zram.sh
exit 0
```

Save the  `/etc/rc.local` file and after rebooting the Raspberry Pi, it will automatically start up with the configured ZRAM memory. 

### How to Remove the overclocking

Remove the line `/usr/bin/zram.sh &` from the `/etc/rc.local` file.

Reboot your Raspberry Pi.

All ZRAM configuration will be disabled after reboot.